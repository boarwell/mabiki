type Fn<T> = (arg?: T) => void;

/** debounce */
export const ikkai = (ms: number) => <T>(fn: Fn<T>): Fn<T> => {
  let id: number | undefined = undefined;

  return (arg?: T) => {
    clearTimeout(id);
    id = setTimeout(() => fn(arg), ms);
  };
};

/** throttle */
export const herasu = (ms: number) => <T>(fn: Fn<T>): Fn<T> => {
  let madaHayai: boolean = false;

  return (arg?: T) => {
    if (madaHayai) {
      return;
    }

    madaHayai = true;
    fn(arg);
    setTimeout(() => {
      madaHayai = false;
    }, ms);
  };
};
